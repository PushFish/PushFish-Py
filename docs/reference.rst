Reference
=========

Interface
---------

Note that all methods can raise :exc:`~pushfish.RequestError` if the server somehow becomes unavailable. They can also raise :exc:`~pushfish.ServerError` if there's a bug in the server, but that's fairly unpredictable and in a perfect world would never be able to happen.

.. autoclass:: pushfish.Service
    :members:
.. autoclass:: pushfish.Device
    :members:
.. autoclass:: pushfish.Subscription()
    :members:
.. autoclass:: pushfish.Message()
    :members:

Custom API instances
--------------------

.. autoclass:: pushfish.Api
    :members:

Exceptions
----------

.. autoexception:: pushfish.PushFishError()
.. autoexception:: pushfish.AccessError()
.. autoexception:: pushfish.NonexistentError()
.. autoexception:: pushfish.SubscriptionError()
.. autoexception:: pushfish.RequestError()
.. autoexception:: pushfish.ServerError()
