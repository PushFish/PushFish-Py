# -*- coding: utf-8 -*-

import sys
import os

# Where pushfish is, so autodoc can find it.
sys.path.insert(0, os.path.abspath('..'))

# Update when the nested classes update is released.
#needs_sphinx = '1.0'
extensions = ['sphinx.ext.autodoc']
source_suffix = '.rst'
master_doc = 'index'

project = u'pushfish (Python API)'
copyright = u'2018, PushFish Team'
author = u'PushFish Team'
version = '1.0'
release = '1.0.0'

language = 'en'
pygments_style = 'sphinx'
todo_include_todos = False

html_theme = 'sphinx_rtd_theme'
html_static_path = ['_static']
html_show_sourcelink = True
html_logo = 'pushfish.png'
htmlhelp_basename = 'PushFishdoc'
