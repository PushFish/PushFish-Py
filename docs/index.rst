**pushfish**
===========

.. code-block:: python
    :emphasize-lines: 3

    import pushfish
    service = pushfish.Service.create("Python PushFish API News")
    service.send("This is a pretty sweet module!")

**pushfish** is the name of a Python module for interfacing with `PushFish <https://push.fish/>`_. Simple and snazzy name, eh? Take a look at the examples and get a running start.

Contents:

.. toctree::
   :maxdepth: 3

   gettingstarted
   reference
