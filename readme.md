PushFish <img src="images/pushfish.png"  width="48" height="48" alt="logo"> (Python API)
===================================================================================

Welcome to `PushFish`, Python's [PushFish](https://push.fish/) ([see the server on GitLab](https://gitlab.com/PushFish/PushFish-api)) API. PushFish lets you push notifications directly to your phone (among other things)! This module lets you do that pushing from Python! It's pretty sweet - it lets you do all sorts of cool things, like integrating notifications into your web app or notifying you when something goes on sale.

And yes, the module's just named `pushfish`. How to install
--------------

The module is [on GitLab](https://gitlab.com/PushFish/PushFish-py) and is also on [PyPI(not yet)](https://pypi.python.org/pypi/pushfish)! That means you can install it using [pip](https://pip.pypa.io/en/latest/installing/). Simply run the following to install it:
``` {.sourceCode .bash}
> pip install pushfish
```
Bam, you're ready to go. It's compatible with both Python 2 and 3, too - nice, huh?

How to use
----------

See the *Getting started* section of the [documentation](http://docs.push.fish/). Here's a little taste:

``` {.sourceCode .python}
import pushfish
import uuid

service = pushfish.Service.create(
    "Open courses", # Name
    "http://example.com/university_icon.png" # Icon URL
)
device = pushfish.Device(uuid.uuid4())
device.subscribe(service)
service.send(
    "A spot is open for you in the competitive eating course!", # Message
    "Course open", # Title
    "http://example.com/courses/eating/competitive" # Link
)
for message in device.get_messages():
    print(message.title)
    print(message.message)
    print(message.link)
```

For information on all the properties of the classes, and on how to use custom API instances, once again see the
[documentation](http://docs.push.fish/).

Contribute
=======

If there's a feature you're missing or a bug you've found in `PushFish`, [open an issue on GitLab](https://gitlab.com/PushFish/PushFish-py/issues/new). If you've got a question - or there's anything you'd like to talk about at all, really - you can reach us via:

-   [Slack](https://join.slack.com/t/fosspush/shared_invite/enQtNDU3MzAzNTEwMTEzLWFhNzVjYWUxNTE5MjZkYzYzZWNjYzdmZDM4ZTc5ZGJjYjYyYTVmYmZkYzgwNTA4NmE4YzMwNjNlNjFjMzY0ZjI)

Enjoy!
